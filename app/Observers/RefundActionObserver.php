<?php

namespace App\Observers;

use App\Notifications\DataChangeEmailNotification;
use App\Refund;
use Illuminate\Support\Facades\Notification;

class RefundActionObserver
{
    public function created(Refund $model)
    {
        $data  = ['action' => 'created', 'model_name' => 'Refund'];
        $users = \App\User::whereHas('roles', function ($q) {
            return $q->where('title', 'Admin');
        })->get();
        Notification::send($users, new DataChangeEmailNotification($data));
    }
}
