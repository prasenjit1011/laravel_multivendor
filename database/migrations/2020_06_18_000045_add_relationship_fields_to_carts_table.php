<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToCartsTable extends Migration
{
    public function up()
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->unsignedInteger('product_id')->nullable();
            $table->foreign('product_id', 'product_fk_1467157')->references('id')->on('products');
            $table->unsignedInteger('order_id')->nullable();
            $table->foreign('order_id', 'order_fk_1467158')->references('id')->on('orders');
        });
    }
}
