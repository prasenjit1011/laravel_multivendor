<?php

namespace App\Http\Requests;

use App\Approval;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreApprovalRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('approval_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [];
    }
}
