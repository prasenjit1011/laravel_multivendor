<?php

namespace App\Observers;

use App\Notifications\DataChangeEmailNotification;
use App\Store;
use Illuminate\Support\Facades\Notification;

class StoreActionObserver
{
    public function created(Store $model)
    {
        $data  = ['action' => 'created', 'model_name' => 'Store'];
        $users = \App\User::whereHas('roles', function ($q) {
            return $q->where('title', 'Admin');
        })->get();
        Notification::send($users, new DataChangeEmailNotification($data));
    }

    public function updated(Store $model)
    {
        $data  = ['action' => 'updated', 'model_name' => 'Store'];
        $users = \App\User::whereHas('roles', function ($q) {
            return $q->where('title', 'Admin');
        })->get();
        Notification::send($users, new DataChangeEmailNotification($data));
    }
}
