@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.saleOffer.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.sale-offers.update", [$saleOffer->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="product_id">{{ trans('cruds.saleOffer.fields.product') }}</label>
                <select class="form-control select2 {{ $errors->has('product') ? 'is-invalid' : '' }}" name="product_id" id="product_id">
                    @foreach($products as $id => $product)
                        <option value="{{ $id }}" {{ ($saleOffer->product ? $saleOffer->product->id : old('product_id')) == $id ? 'selected' : '' }}>{{ $product }}</option>
                    @endforeach
                </select>
                @if($errors->has('product'))
                    <span class="text-danger">{{ $errors->first('product') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.saleOffer.fields.product_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="started_at">{{ trans('cruds.saleOffer.fields.started_at') }}</label>
                <input class="form-control date {{ $errors->has('started_at') ? 'is-invalid' : '' }}" type="text" name="started_at" id="started_at" value="{{ old('started_at', $saleOffer->started_at) }}">
                @if($errors->has('started_at'))
                    <span class="text-danger">{{ $errors->first('started_at') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.saleOffer.fields.started_at_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="ended_at">{{ trans('cruds.saleOffer.fields.ended_at') }}</label>
                <input class="form-control date {{ $errors->has('ended_at') ? 'is-invalid' : '' }}" type="text" name="ended_at" id="ended_at" value="{{ old('ended_at', $saleOffer->ended_at) }}">
                @if($errors->has('ended_at'))
                    <span class="text-danger">{{ $errors->first('ended_at') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.saleOffer.fields.ended_at_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="title">{{ trans('cruds.saleOffer.fields.title') }}</label>
                <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" value="{{ old('title', $saleOffer->title) }}" required>
                @if($errors->has('title'))
                    <span class="text-danger">{{ $errors->first('title') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.saleOffer.fields.title_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="discount_percentage">{{ trans('cruds.saleOffer.fields.discount_percentage') }}</label>
                <input class="form-control {{ $errors->has('discount_percentage') ? 'is-invalid' : '' }}" type="number" name="discount_percentage" id="discount_percentage" value="{{ old('discount_percentage', $saleOffer->discount_percentage) }}" step="1" required>
                @if($errors->has('discount_percentage'))
                    <span class="text-danger">{{ $errors->first('discount_percentage') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.saleOffer.fields.discount_percentage_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection