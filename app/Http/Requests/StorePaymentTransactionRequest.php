<?php

namespace App\Http\Requests;

use App\PaymentTransaction;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StorePaymentTransactionRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('payment_transaction_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [];
    }
}
