<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroySaleOfferRequest;
use App\Http\Requests\StoreSaleOfferRequest;
use App\Http\Requests\UpdateSaleOfferRequest;
use App\Product;
use App\SaleOffer;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SaleOffersController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('sale_offer_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $saleOffers = SaleOffer::all();

        return view('admin.saleOffers.index', compact('saleOffers'));
    }

    public function create()
    {
        abort_if(Gate::denies('sale_offer_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $products = Product::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.saleOffers.create', compact('products'));
    }

    public function store(StoreSaleOfferRequest $request)
    {
        $saleOffer = SaleOffer::create($request->all());

        return redirect()->route('admin.sale-offers.index');
    }

    public function edit(SaleOffer $saleOffer)
    {
        abort_if(Gate::denies('sale_offer_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $products = Product::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $saleOffer->load('product');

        return view('admin.saleOffers.edit', compact('products', 'saleOffer'));
    }

    public function update(UpdateSaleOfferRequest $request, SaleOffer $saleOffer)
    {
        $saleOffer->update($request->all());

        return redirect()->route('admin.sale-offers.index');
    }

    public function show(SaleOffer $saleOffer)
    {
        abort_if(Gate::denies('sale_offer_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $saleOffer->load('product');

        return view('admin.saleOffers.show', compact('saleOffer'));
    }

    public function destroy(SaleOffer $saleOffer)
    {
        abort_if(Gate::denies('sale_offer_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $saleOffer->delete();

        return back();
    }

    public function massDestroy(MassDestroySaleOfferRequest $request)
    {
        SaleOffer::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
