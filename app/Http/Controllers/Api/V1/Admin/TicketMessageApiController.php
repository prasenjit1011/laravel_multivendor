<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreTicketMessageRequest;
use App\Http\Requests\UpdateTicketMessageRequest;
use App\Http\Resources\Admin\TicketMessageResource;
use App\TicketMessage;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TicketMessageApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('ticket_message_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TicketMessageResource(TicketMessage::with(['ticket', 'user'])->get());
    }

    public function store(StoreTicketMessageRequest $request)
    {
        $ticketMessage = TicketMessage::create($request->all());

        return (new TicketMessageResource($ticketMessage))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(TicketMessage $ticketMessage)
    {
        abort_if(Gate::denies('ticket_message_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TicketMessageResource($ticketMessage->load(['ticket', 'user']));
    }

    public function update(UpdateTicketMessageRequest $request, TicketMessage $ticketMessage)
    {
        $ticketMessage->update($request->all());

        return (new TicketMessageResource($ticketMessage))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(TicketMessage $ticketMessage)
    {
        abort_if(Gate::denies('ticket_message_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $ticketMessage->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
