<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'                 => 1,
                'name'               => 'Admin',
                'email'              => 'admin@admin.com',
                'password'           => '$2y$10$XAIDWdN/5PTwhF75f5H4BOuqQcyIdc2M.nePkRmViCigI0zBtM1fG',
                'remember_token'     => null,
                'verified'           => 1,
                'verified_at'        => '2020-05-13 23:36:08',
                'verification_token' => '',
            ],
        ];

        User::insert($users);
    }
}
