<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaleOffersTable extends Migration
{
    public function up()
    {
        Schema::create('sale_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->date('started_at')->nullable();
            $table->date('ended_at')->nullable();
            $table->string('title');
            $table->integer('discount_percentage');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
