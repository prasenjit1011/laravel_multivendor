<?php

namespace App;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class PaymentTransaction extends Model
{
    use SoftDeletes, Auditable;

    public $table = 'payment_transactions';

    public static $searchable = [
        'transaction',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'product_id',
        'transaction',
        'gateway_response',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public static function boot()
    {
        parent::boot();
        PaymentTransaction::observe(new \App\Observers\PaymentTransactionActionObserver);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
