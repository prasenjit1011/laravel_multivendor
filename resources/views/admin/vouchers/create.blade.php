@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.voucher.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.vouchers.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">{{ trans('cruds.voucher.fields.title') }}</label>
                <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" value="{{ old('title', '') }}">
                @if($errors->has('title'))
                    <span class="text-danger">{{ $errors->first('title') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.voucher.fields.title_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="category_id">{{ trans('cruds.voucher.fields.category') }}</label>
                <select class="form-control select2 {{ $errors->has('category') ? 'is-invalid' : '' }}" name="category_id" id="category_id">
                    @foreach($categories as $id => $category)
                        <option value="{{ $id }}" {{ old('category_id') == $id ? 'selected' : '' }}>{{ $category }}</option>
                    @endforeach
                </select>
                @if($errors->has('category'))
                    <span class="text-danger">{{ $errors->first('category') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.voucher.fields.category_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="percentage">{{ trans('cruds.voucher.fields.percentage') }}</label>
                <input class="form-control {{ $errors->has('percentage') ? 'is-invalid' : '' }}" type="text" name="percentage" id="percentage" value="{{ old('percentage', '') }}">
                @if($errors->has('percentage'))
                    <span class="text-danger">{{ $errors->first('percentage') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.voucher.fields.percentage_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="expired_at">{{ trans('cruds.voucher.fields.expired_at') }}</label>
                <input class="form-control date {{ $errors->has('expired_at') ? 'is-invalid' : '' }}" type="text" name="expired_at" id="expired_at" value="{{ old('expired_at') }}">
                @if($errors->has('expired_at'))
                    <span class="text-danger">{{ $errors->first('expired_at') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.voucher.fields.expired_at_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="max_usage">{{ trans('cruds.voucher.fields.max_usage') }}</label>
                <input class="form-control {{ $errors->has('max_usage') ? 'is-invalid' : '' }}" type="text" name="max_usage" id="max_usage" value="{{ old('max_usage', '') }}">
                @if($errors->has('max_usage'))
                    <span class="text-danger">{{ $errors->first('max_usage') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.voucher.fields.max_usage_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="store_id">{{ trans('cruds.voucher.fields.store') }}</label>
                <select class="form-control select2 {{ $errors->has('store') ? 'is-invalid' : '' }}" name="store_id" id="store_id">
                    @foreach($stores as $id => $store)
                        <option value="{{ $id }}" {{ old('store_id') == $id ? 'selected' : '' }}>{{ $store }}</option>
                    @endforeach
                </select>
                @if($errors->has('store'))
                    <span class="text-danger">{{ $errors->first('store') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.voucher.fields.store_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection