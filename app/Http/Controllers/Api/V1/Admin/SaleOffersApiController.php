<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSaleOfferRequest;
use App\Http\Requests\UpdateSaleOfferRequest;
use App\Http\Resources\Admin\SaleOfferResource;
use App\SaleOffer;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SaleOffersApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('sale_offer_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SaleOfferResource(SaleOffer::with(['product'])->get());
    }

    public function store(StoreSaleOfferRequest $request)
    {
        $saleOffer = SaleOffer::create($request->all());

        return (new SaleOfferResource($saleOffer))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(SaleOffer $saleOffer)
    {
        abort_if(Gate::denies('sale_offer_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SaleOfferResource($saleOffer->load(['product']));
    }

    public function update(UpdateSaleOfferRequest $request, SaleOffer $saleOffer)
    {
        $saleOffer->update($request->all());

        return (new SaleOfferResource($saleOffer))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(SaleOffer $saleOffer)
    {
        abort_if(Gate::denies('sale_offer_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $saleOffer->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
