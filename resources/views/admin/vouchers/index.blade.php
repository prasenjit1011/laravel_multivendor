@extends('layouts.admin')
@section('content')
@can('voucher_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.vouchers.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.voucher.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.voucher.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Voucher">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.voucher.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.voucher.fields.title') }}
                        </th>
                        <th>
                            {{ trans('cruds.voucher.fields.category') }}
                        </th>
                        <th>
                            {{ trans('cruds.voucher.fields.percentage') }}
                        </th>
                        <th>
                            {{ trans('cruds.voucher.fields.expired_at') }}
                        </th>
                        <th>
                            {{ trans('cruds.voucher.fields.max_usage') }}
                        </th>
                        <th>
                            {{ trans('cruds.voucher.fields.store') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($vouchers as $key => $voucher)
                        <tr data-entry-id="{{ $voucher->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $voucher->id ?? '' }}
                            </td>
                            <td>
                                {{ $voucher->title ?? '' }}
                            </td>
                            <td>
                                {{ $voucher->category->name ?? '' }}
                            </td>
                            <td>
                                {{ $voucher->percentage ?? '' }}
                            </td>
                            <td>
                                {{ $voucher->expired_at ?? '' }}
                            </td>
                            <td>
                                {{ $voucher->max_usage ?? '' }}
                            </td>
                            <td>
                                {{ $voucher->store->title ?? '' }}
                            </td>
                            <td>
                                @can('voucher_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.vouchers.show', $voucher->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('voucher_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.vouchers.edit', $voucher->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('voucher_delete')
                                    <form action="{{ route('admin.vouchers.destroy', $voucher->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('voucher_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.vouchers.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Voucher:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection