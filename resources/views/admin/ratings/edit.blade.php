@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.rating.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.ratings.update", [$rating->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="ratingable_type">{{ trans('cruds.rating.fields.ratingable_type') }}</label>
                <input class="form-control {{ $errors->has('ratingable_type') ? 'is-invalid' : '' }}" type="text" name="ratingable_type" id="ratingable_type" value="{{ old('ratingable_type', $rating->ratingable_type) }}">
                @if($errors->has('ratingable_type'))
                    <span class="text-danger">{{ $errors->first('ratingable_type') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.rating.fields.ratingable_type_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="ratingable">{{ trans('cruds.rating.fields.ratingable') }}</label>
                <input class="form-control {{ $errors->has('ratingable') ? 'is-invalid' : '' }}" type="number" name="ratingable" id="ratingable" value="{{ old('ratingable', $rating->ratingable) }}" step="1">
                @if($errors->has('ratingable'))
                    <span class="text-danger">{{ $errors->first('ratingable') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.rating.fields.ratingable_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="ratings">{{ trans('cruds.rating.fields.ratings') }}</label>
                <input class="form-control {{ $errors->has('ratings') ? 'is-invalid' : '' }}" type="number" name="ratings" id="ratings" value="{{ old('ratings', $rating->ratings) }}" step="1">
                @if($errors->has('ratings'))
                    <span class="text-danger">{{ $errors->first('ratings') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.rating.fields.ratings_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection