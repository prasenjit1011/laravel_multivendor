<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToProductsTable extends Migration
{
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedInteger('language_id')->nullable();
            $table->foreign('language_id', 'language_fk_1467187')->references('id')->on('languages');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id', 'user_fk_1470603')->references('id')->on('users');
        });
    }
}
