<?php

namespace App\Observers;

use App\Notifications\DataChangeEmailNotification;
use App\Order;
use Illuminate\Support\Facades\Notification;

class OrderActionObserver
{
    public function updated(Order $model)
    {
        $data  = ['action' => 'updated', 'model_name' => 'Order'];
        $users = \App\User::whereHas('roles', function ($q) {
            return $q->where('title', 'Admin');
        })->get();
        Notification::send($users, new DataChangeEmailNotification($data));
    }
}
