<?php

namespace App\Http\Requests;

use App\ProductLanguage;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreProductLanguageRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('product_language_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'product_id'  => [
                'required',
                'integer',
            ],
            'title'       => [
                'required',
            ],
            'description' => [
                'required',
            ],
        ];
    }
}
