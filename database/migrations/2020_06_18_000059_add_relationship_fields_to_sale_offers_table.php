<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToSaleOffersTable extends Migration
{
    public function up()
    {
        Schema::table('sale_offers', function (Blueprint $table) {
            $table->unsignedInteger('product_id')->nullable();
            $table->foreign('product_id', 'product_fk_1467201')->references('id')->on('products');
        });
    }
}
