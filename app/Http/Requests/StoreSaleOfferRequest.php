<?php

namespace App\Http\Requests;

use App\SaleOffer;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreSaleOfferRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('sale_offer_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'started_at'          => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
            'ended_at'            => [
                'date_format:' . config('panel.date_format'),
                'nullable',
            ],
            'title'               => [
                'required',
            ],
            'discount_percentage' => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
