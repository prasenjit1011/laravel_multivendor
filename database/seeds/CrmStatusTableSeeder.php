<?php

use App\CrmStatus;
use Illuminate\Database\Seeder;

class CrmStatusTableSeeder extends Seeder
{
    public function run()
    {
        $crmStatuses = [
            [
                'id'         => '1',
                'name'       => 'Lead',
                'created_at' => '2020-05-13 17:42:18',
                'updated_at' => '2020-05-13 17:42:18',
            ],
            [
                'id'         => '2',
                'name'       => 'Customer',
                'created_at' => '2020-05-13 17:42:18',
                'updated_at' => '2020-05-13 17:42:18',
            ],
            [
                'id'         => '3',
                'name'       => 'Partner',
                'created_at' => '2020-05-13 17:42:18',
                'updated_at' => '2020-05-13 17:42:18',
            ],
        ];

        CrmStatus::insert($crmStatuses);
    }
}
