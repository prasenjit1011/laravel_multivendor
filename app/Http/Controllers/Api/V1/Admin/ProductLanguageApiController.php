<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreProductLanguageRequest;
use App\Http\Requests\UpdateProductLanguageRequest;
use App\Http\Resources\Admin\ProductLanguageResource;
use App\ProductLanguage;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductLanguageApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('product_language_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ProductLanguageResource(ProductLanguage::with(['product', 'language'])->get());
    }

    public function store(StoreProductLanguageRequest $request)
    {
        $productLanguage = ProductLanguage::create($request->all());

        return (new ProductLanguageResource($productLanguage))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(ProductLanguage $productLanguage)
    {
        abort_if(Gate::denies('product_language_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ProductLanguageResource($productLanguage->load(['product', 'language']));
    }

    public function update(UpdateProductLanguageRequest $request, ProductLanguage $productLanguage)
    {
        $productLanguage->update($request->all());

        return (new ProductLanguageResource($productLanguage))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(ProductLanguage $productLanguage)
    {
        abort_if(Gate::denies('product_language_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $productLanguage->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
