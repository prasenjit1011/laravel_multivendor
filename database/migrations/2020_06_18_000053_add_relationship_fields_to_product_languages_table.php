<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToProductLanguagesTable extends Migration
{
    public function up()
    {
        Schema::table('product_languages', function (Blueprint $table) {
            $table->unsignedInteger('product_id');
            $table->foreign('product_id', 'product_fk_1467191')->references('id')->on('products');
            $table->unsignedInteger('language_id')->nullable();
            $table->foreign('language_id', 'language_fk_1467192')->references('id')->on('languages');
        });
    }
}
