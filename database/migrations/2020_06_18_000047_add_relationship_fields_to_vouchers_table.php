<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToVouchersTable extends Migration
{
    public function up()
    {
        Schema::table('vouchers', function (Blueprint $table) {
            $table->unsignedInteger('category_id')->nullable();
            $table->foreign('category_id', 'category_fk_1467296')->references('id')->on('product_categories');
            $table->unsignedInteger('store_id')->nullable();
            $table->foreign('store_id', 'store_fk_1470725')->references('id')->on('stores');
        });
    }
}
