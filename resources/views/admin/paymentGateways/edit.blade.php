@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.paymentGateway.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.payment-gateways.update", [$paymentGateway->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label>{{ trans('cruds.paymentGateway.fields.status') }}</label>
                <select class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" id="status">
                    <option value disabled {{ old('status', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\PaymentGateway::STATUS_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('status', $paymentGateway->status) === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('status'))
                    <span class="text-danger">{{ $errors->first('status') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.paymentGateway.fields.status_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="title">{{ trans('cruds.paymentGateway.fields.title') }}</label>
                <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" value="{{ old('title', $paymentGateway->title) }}" required>
                @if($errors->has('title'))
                    <span class="text-danger">{{ $errors->first('title') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.paymentGateway.fields.title_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="settings">{{ trans('cruds.paymentGateway.fields.settings') }}</label>
                <textarea class="form-control {{ $errors->has('settings') ? 'is-invalid' : '' }}" name="settings" id="settings">{{ old('settings', $paymentGateway->settings) }}</textarea>
                @if($errors->has('settings'))
                    <span class="text-danger">{{ $errors->first('settings') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.paymentGateway.fields.settings_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="is_payable">{{ trans('cruds.paymentGateway.fields.is_payable') }}</label>
                <input class="form-control {{ $errors->has('is_payable') ? 'is-invalid' : '' }}" type="text" name="is_payable" id="is_payable" value="{{ old('is_payable', $paymentGateway->is_payable) }}">
                @if($errors->has('is_payable'))
                    <span class="text-danger">{{ $errors->first('is_payable') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.paymentGateway.fields.is_payable_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection