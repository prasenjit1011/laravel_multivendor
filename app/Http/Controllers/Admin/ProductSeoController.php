<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyProductSeoRequest;
use App\Http\Requests\StoreProductSeoRequest;
use App\Http\Requests\UpdateProductSeoRequest;
use App\Language;
use App\Product;
use App\ProductSeo;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductSeoController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('product_seo_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $productSeos = ProductSeo::all();

        return view('admin.productSeos.index', compact('productSeos'));
    }

    public function create()
    {
        abort_if(Gate::denies('product_seo_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $products = Product::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $languages = Language::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.productSeos.create', compact('products', 'languages'));
    }

    public function store(StoreProductSeoRequest $request)
    {
        $productSeo = ProductSeo::create($request->all());

        return redirect()->route('admin.product-seos.index');
    }

    public function edit(ProductSeo $productSeo)
    {
        abort_if(Gate::denies('product_seo_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $products = Product::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $languages = Language::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        $productSeo->load('product', 'language');

        return view('admin.productSeos.edit', compact('products', 'languages', 'productSeo'));
    }

    public function update(UpdateProductSeoRequest $request, ProductSeo $productSeo)
    {
        $productSeo->update($request->all());

        return redirect()->route('admin.product-seos.index');
    }

    public function show(ProductSeo $productSeo)
    {
        abort_if(Gate::denies('product_seo_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $productSeo->load('product', 'language');

        return view('admin.productSeos.show', compact('productSeo'));
    }

    public function destroy(ProductSeo $productSeo)
    {
        abort_if(Gate::denies('product_seo_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $productSeo->delete();

        return back();
    }

    public function massDestroy(MassDestroyProductSeoRequest $request)
    {
        ProductSeo::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
